/*
  Authors : Srikrishna Sasidharan
  Future work
*/
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-cart-stepper',
  templateUrl: './cart-stepper.component.html',
  styleUrls: ['./cart-stepper.component.css']
})
export class CartStepperComponent implements OnInit {
  isLinear = true;
  isOptional = true;
  isAddress1Removed:boolean=true;
  isAddress2Removed:boolean=true;
  ELEMENT_DATA: Cards[] = [
    {  cardEnding: 'Visa ending with 1234',name: 'Srikrishna', valid: '02/24'},
    {  cardEnding:'Visa ending with 1234',name: 'Srikrishna', valid: '02/24'},
    
  ];
  displayedColumns: string[] = [ 'cardEnding', 'name', 'valid'];
  dataSource = this.ELEMENT_DATA;

  cardNumberControl = new FormControl('', [Validators.required]);
  cardNameControl = new FormControl('', [Validators.required]);
  expYearControl = new FormControl('', [Validators.required]);
  cvvControl = new FormControl('', [Validators.required]);

  expMonthControl = new FormControl('', [Validators.required]);
  selectFormControl = new FormControl('', Validators.required);
  expMonths: NewCardExpMonth[] = [
    {month:1, monthVal:1},
    {month:2, monthVal:2},
    {month:3, monthVal:3},
    {month:4, monthVal:4},
    {month:5, monthVal:5},
    {month:6, monthVal:6},
    {month:7, monthVal:7},
    {month:8, monthVal:8},
    {month:9, monthVal:9},
    {month:10, monthVal:10},
    {month:11, monthVal:11},
    {month:12, monthVal:12}

    
  ];

  expYears: NewCardExpMonth[] = [
    {month:2020, monthVal:2020},
    {month:2021, monthVal:2021},
    {month:2022, monthVal:2022},
    {month:2023, monthVal:2023},
    {month:2024, monthVal:2024},
    {month:2025, monthVal:2025},
    {month:2026, monthVal:2026},
    {month:2027, monthVal:2027},
    {month:2028, monthVal:2028},
    {month:2029, monthVal:2029},
    {month:2030, monthVal:2030},
    {month:2031, monthVal:2031},
    {month:2032, monthVal:2032},
    {month:2033, monthVal:2033},
    {month:2034, monthVal:2034},

  ];

  constructor() { }

  ngOnInit() {
  }
  RemoveClicked1(){
    this.isAddress1Removed = false;
  }
  RemoveClicked2(){
    this.isAddress2Removed = false;
  }
  addNewAddressConfirm(){

    
  }
}
export interface Cards {
  cardEnding: string;
  name: string;
  valid: string;
}
export interface NewCardExpMonth {
  month: number;
  monthVal: number;
}
