/*
Authors : Thanigaiselvan Senthil Shanmugham,Hari Arunachalam,Srikrishna Sasidharan
*/


import { Component, OnInit, Input } from '@angular/core';
import { CartService } from '../services/cart-service';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { SearchService } from '../services/search-services';

import { debounceTime, switchMap } from 'rxjs/operators';
import { of } from 'rxjs'

@Component({
  selector: 'app-user-navigation-bar',
  templateUrl: './user-navigation-bar.component.html',
  styleUrls: ['./user-navigation-bar.component.css']
})
export class UserNavigationBarComponent implements OnInit {
  myControl = new FormControl();
  cartCount: number = 0;
  @Input() cartItem: string;
  @Input() loggedIn: string;
  loginShow = true;
  searchSwitch = true;
  autoSwitch = true;
  adminShow = false;
  constructor(private route: Router, private cartService: CartService, private searchService: SearchService) {
    //   sessionStorage.setItem('uid',this.cartCount.toString())
  }
  fg: FormGroup;
  searchResults

  onActivate(componentReference) {


    //Below will subscribe to the searchItem emitter
    // Will receive the data from child here 

    // if (componentReference.searchItem) {

    //   componentReference.searchItem.subscribe((data) => {
    //     
    //     localStorage.setItem('currentUser', "true");

    //     localStorage.getItem('currentUser');
    //     if (localStorage.getItem('currentUser') === "true") {
    //       this.userStatus = true;
    //     }
    //   });
    // }
    // if (componentReference.cartItem) {
    //   componentReference.cartItem.subscribe((data) => {
    //     
    //     this.cartCount = localStorage.getItem('cartCount');
    //   });
    // }

    if (componentReference.cartItem) {
      componentReference.cartItem.subscribe((data) => {

        this.cartCount = + data
        //  this.cartCount = localStorage.getItem('cartCount');
      });
    }
    if (componentReference.loggedIn) {
      componentReference.loggedIn.subscribe((data) => {

        this.loginShow = false;
        var isAdmin = sessionStorage.getItem('isAdmin')
        if (isAdmin && isAdmin == '1') {
          this.adminShow = true;
        }
      });
    }
  }

  onLoggedIn(componentReference) {

    if (componentReference.cartItem) {
      componentReference.cartItem.subscribe((data) => {

        //this.cartCount = localStorage.getItem('cartCount');

        var uid = sessionStorage.getItem('uid')
        if (uid == null || uid == undefined) {
          this.cartCount = + sessionStorage.getItem('cartCount')

        } else {
          this.cartService.getCartCount({ 'uid': uid }).subscribe(data => {
            this.cartCount = data.count;

          })
        }
      });
    }

  }
  ngOnInit() {
    var uid = sessionStorage.getItem('uid')
    if (uid == "null" || uid == undefined) {
      for (var key in sessionStorage) {
        if (key.indexOf('cartItem') !== -1) {
          this.cartCount += 1

        }
      }
      sessionStorage.setItem('cartCount', this.cartCount.toString())
    }
    else {
      this.loginShow = false;

      this.cartService.getCartCount({ 'uid': uid }).subscribe(data => {
        this.cartCount = data.count;

      })
    }
    if (this.cartCount == undefined || this.cartCount == null) {
      this.cartCount = 0;
    }


    this.fg = new FormGroup({
      'searchText': new FormControl()
    })
    var isAdmin = sessionStorage.getItem('isAdmin')
    if (isAdmin && isAdmin == '1') {
      this.adminShow = true;
    }
    this.autoSuggestion();
  }

  //Auto suggestion
  autoSuggestion() {
    this.searchResults = this.fg.get('searchText').valueChanges
      .pipe(
        debounceTime(300),
        switchMap(value => {
          if (this.fg.get('searchText').value && this.fg.get('searchText').value != '') {
            let body = {
              'text': this.fg.get('searchText').value,
              'start': 0,
              'end': 5
            }
            return this.searchService.search(body)
          }

          else
            return of([])
        })

      )
  }

  filter(res): any {
    var prod = res.products
    return prod
  }

  login() {
    this.route.navigate(["/", "login"]);
  }

  order() {
    this.route.navigate(["/", "order"]);
  }
  register() {
    this.route.navigate(["/", "register"]);
  }

  logout() {
    this.loginShow = true;
    this.adminShow = false;
    sessionStorage.removeItem('jwt');
    sessionStorage.removeItem('uid');
    this.cartCount = 0;
    sessionStorage.clear();
    this.route.navigate(["/", "homepage"]);
  }

  userProfile() {
    this.route.navigate(["/", "userProfile"]);
  }


  cart() {
    this.route.navigate(["/", "cart"]);
  }

  //Search 
  search() {
    document.getElementById('searchBar').blur()
    this.searchResults = of([])
    this.autoSuggestion();
    if (this.searchSwitch) {
      this.searchSwitch = !this.searchSwitch
      this.route.navigate(["/", "searchResults", this.fg.get('searchText').value]);
    } else {
      this.searchSwitch = !this.searchSwitch
      this.route.navigate(["/", "searchResult", this.fg.get('searchText').value]);
    }
  }

  admin() {
    this.route.navigate(["/", "adminHome"]);
  }

  goToProduct(user) {


    this.route.navigate(["", "product", user.productID])
  }

  //navigate from autosuggestion
  goToProduct2(user) {
    
    let body = {
      'id': user.Product
    }
    this.searchService.getProductID(body).subscribe(data => {
      let prod = data.productID
      
      if (this.autoSwitch) {
        this.autoSwitch = !this.autoSwitch;
        this.route.navigate(["", "product", prod[0].productID])
      } else {
        this.autoSwitch = !this.autoSwitch;
        this.route.navigate(["", "prod", prod[0].productID])
      }
    })

  }
  homepage() {
    this.searchResults = this.fg.get('searchText').setValue('');
    this.autoSuggestion();
    this.route.navigate(["/", "homepage"]);
  }
}
