/*
  Authors: Sreyas Naaraayanan Ramanathan
*/
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import {AuthService} from '../services/authentication-service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registrationForm:FormGroup;
  onInit;
  constructor(private route: Router,private authService:AuthService,private _snackBar: MatSnackBar) { }
  password: String;
  ngOnInit() {
    this.onInit = false;
    this.registrationForm = new FormGroup({
      'firstName': new FormControl('', { validators: [Validators.required, Validators.minLength(2), Validators.maxLength(30)] }),
      'lastName': new FormControl('', { validators: [Validators.required, , Validators.minLength(2), Validators.maxLength(30)] }),
      'email': new FormControl('', { validators: [Validators.required] }),
      'password': new FormControl('', { validators: [Validators.required, Validators.minLength(6), Validators.maxLength(20)] }),
      'addressLine1':new FormControl(''),
      'addressLine2':new FormControl(''),
      'phoneNumber':new FormControl('',{ validators: [Validators.required, Validators.minLength(7),Validators.maxLength(10)] }),
      'city':new FormControl(''),
      'state':new FormControl(''),
      'postcode':new FormControl('')

    })
  }

  onSubmit() {
   
  }

  register() {
    if(this.registrationForm.invalid){
      this.onInit = true;
    }else{    

      let body ={
        'firstName':this.registrationForm.get('firstName').value,
        'lastName':this.registrationForm.get('lastName').value,
        'email':this.registrationForm.get('email').value,
        'password':this.registrationForm.get('password').value,
        'addressLine1':this.registrationForm.get('addressLine1').value,
        'addressLine2':this.registrationForm.get('addressLine2').value,
        'phoneNumber':this.registrationForm.get('phoneNumber').value,
        'city':this.registrationForm.get('city').value,
        'state':this.registrationForm.get('state').value,
        'postcode':this.registrationForm.get('postcode').value
      }

      this.authService.registration(body).subscribe(data=>{

        if (data.success == true) {
          this.route.navigate(["", "login"]);
        }
        
        else if(data.success == "alreadyRegistered"){

          this.route.navigate(["","login"]);

        }

        else {

          this._snackBar.open(data.message, "Please retry", {
            duration: 1500,
          });
        }
       

      })



      //this.route.navigate(["", "homepage"]);
  }
  }


}
