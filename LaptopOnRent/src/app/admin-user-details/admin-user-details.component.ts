/*
  Authors: Hari Arunachalam
*/
import { Component, OnInit } from '@angular/core';
import {ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource,MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import { AdminService } from '../services/admin.services';
const NAMES = ['Thanigai','Selvan','Sreyas','Narayanan','Hari','Arunachalam','Sri','Sai','Krishna'];
@Component({
  selector: 'app-admin-user-details',
  templateUrl: './admin-user-details.component.html',
  styleUrls: ['./admin-user-details.component.css']
})
export class AdminUserDetailsComponent implements OnInit {
  displayedColumns = ['id','name','email','userStatus','action'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator,{static: true}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: true}) sort: MatSort;
  //Below code inspired from the online source on the stackblitz and have modified based on the need
  //https://stackblitz.com/angular/dnbermjydavk?file=app%2Ftable-overview-example.ts
  constructor(private route:Router,private adminService:AdminService,private _snackBar: MatSnackBar) { 
    // const users: UserData[] = [];
    // for (let i = 1; i <= 100; i++) 
    // { 
    //   users.push(createNewUser(i)); 
    // }
    // this.dataSource = new MatTableDataSource(users);
  }

  ngOnInit() {
    this.getUsers();
  }

  ngAfterViewInit() {
    //this.dataSource.paginator = this.paginator;
    
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }

  userdetails()
  {
    this.route.navigate(["/","userdetails"]);
  }

  getUsers (){
    this.adminService.getUsers().subscribe(data=>{
     
      let users =data.data
      users.forEach(element => {
        if(element.isLocked == "0"){
          element.isLocked = false
        }else{
          element.isLocked = true
        }
      });
      this.dataSource = new MatTableDataSource(users);
      this.dataSource.sort = this.sort;
    })
  }

  //Lock and unlock user
  lock(user) {
    
    this.adminService.lockUser(user).subscribe(data=>{
      if(data.success)
      {
        let users =this.dataSource.data
        users.forEach(element => {
        if(element.uId == user.uId){
          element.isLocked =!element.isLocked
        }
       });
       this.dataSource = new MatTableDataSource(users);
        this._snackBar.open("User status updated", "ok", {
          duration: 2000,
        });

      }
    })
  }

}
// function createNewUser(id: number): UserData {
//   const name = NAMES[id % 9] ;

//   return {
//     id: id.toString(),
//     name: name
//   };

  
// }

export interface UserData {
  id: string;
  name: string;
}
