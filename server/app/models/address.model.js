/*
    Authors: Srikrishna Sasidharan
*/
var sql = require('../../db.js');

var Address = function(address){
    this.firstname = address.firstname;
    this.lastname = address.lastname;
    this.uid = address.uid;
    this.addressId = address.addressId;
    this.addressline1 = address.addressline1;
    this.addressline2 = address.addressline2;
    this.phone = address.phone;
    this.city = address.city;
    this.state = address.state;
    this.postcode = address.postcode;
}

Address.addNewAddress = function(address,result){
    sql.query("select max(addressId) as c from address", function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            var addrCount;
            if(res[0].c=="NULL" || res[0].c == null){
                 addrCount = 1;
            }else{
             addrCount = res[0].c +1;
        }
            var quer = "insert into address(firstname,lastname,addressid,uid,phone,addressline1,addressline2,city,state,postcode)"
            +" VALUES ('"+address.firstname+"','"+address.lastname+"','"+addrCount+"', '"+address.uid+"', '"+address.phone+"', '"+address.addressline1+"', '"+address.addressline2
            +"','"+address.city+"','"+address.state+"','"+address.postcode+"')";

            sql.query(quer, function(error, response){
                if(error){
                    console.log("error :",error);
                    result(error,null);
                }else{
                    result(null,response);
                }
            } );
        }
})
}

Address.getAddressForUser = function(address, result){

    sql.query("select * from address where uid= ? ",address.uid,function(error, response){
        if(error){
            console.log("error :",error);
            result(error,null);
        }else{
            result(null,response);
        }
    })
}


Address.removeAddress = function(address, result){
    sql.query("delete from address where uid='"+address.uid+"' and addressId='"+address.addressId+"'", function(error, response){
        if(error){
            console.log("error :",error);
            result(error,null);
        }else{
            result(null,response);
        }
    } )
}


Address.editAddress = function(address, result){
  

    sql.query("update  address set addressLine1='"+address.addressline1+"' ,addressline2='"+address.addressline2
    +"', city='"+address.city+"', state='"+address.state+"', postcode='"+address.postcode+"',firstname='"
    +address.firstname+"',lastname='"+address.lastname+"',phone='"+address.phone
    +"'   where addressId="+address.addressId+" and uid='"+address.uid+"'",function(error, response){
        if(error){
            console.log("error :",error);
            result(error,null);
        }else{
            result(null,response);
        }
    })
}
module.exports= Address;