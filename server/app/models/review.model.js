/*
    Author: Thanigaiselvan senthil shanmugam
*/
const mongoose = require('mongoose');

//defining the schema 'reviews'
const reviewSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId, 
    uid:Number,
    productID: Number,
    userName: String,
    star: Number,
    review: String
});

//exporting the schema
module.exports = mongoose.model("reviews", reviewSchema);

