/*
    Authors: Srikrishna Sasidharan
*/
var sql = require('../../db.js');

var Payment = function(payment){
    this.uid = payment.uid
    this.paymentid = payment.paymentid
    this.cardNumber = payment.cardNumber
    this.name = payment.name
    this.expMonth = payment.expMonth
    this.expYear = payment.expYear
    this.type = payment.type


}



Payment.addNewPayment = function(payment,result){
    sql.query("select max(paymentid) as c from payment", function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            var payCount;
            if(res[0].c=="NULL" || res[0].c == null){
                payCount = 1;
            }else{
                payCount = res[0].c +1;
        }
            var quer = "insert into payment(uid,paymentid,cardNumber,name,expMonth,expYear,type)"
            +" VALUES ('"+payment.uid+"','"+payCount+"','"+payment.cardNumber+"','"
            +payment.name+"','"+payment.expMonth+"','"+payment.expYear+"','"+payment.type+"')";

            sql.query(quer, function(error, response){
                if(error){
                    console.log("error :",error);
                    result(error,null);
                }else{
                    result(null,response);
                }
            } );
        }
})
}


Payment.getPaymentForUser = function(payment, result){

    sql.query("select * from payment where uid= ? ",payment.uid,function(error, response){
        if(error){
            console.log("error :",error);
            result(error,null);
        }else{
            result(null,response);
        }
    })
}




module.exports= Payment;
