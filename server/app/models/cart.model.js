/*
    Authors: Srikrishna Sasidharan
*/
var sql = require('../../db.js');

var Cart = function(cart){
    this.uid = cart.uid;
    this.productID = cart.productID;
    this.quantity = cart.quantity;
    this.cartId = cart.cartId;
    this.softwares = cart.softwares;
    this.fromDate = cart.fromDate;
    this.toDate = cart.toDate;
    this.maxQuantity = cart.maxQuantity
}
Cart.addToCartForUser1 = function (cart,result){
    sql.query("Select max(cartId) as c from cart ", function (error, response) {             
        if(error) {
            console.log("error: ", error);
            result(response, null);
        }
        else{
           if(response[0].c == "NULL" || response[0].c == null){
               cart.cartId = 1;
           } else{
               cart.cartId = response[0].c +1;
           }
           response[0].uid = cart.uid
            var quer = "insert into cart(uid,productID,quantity,cartId,fromDate,toDate,maxQuantity) VALUES ('"
            +cart.uid+"', '"+cart.productID+"', '"+cart.quantity+"', '"+cart.cartId+"','"+cart.fromDate+"','"
            +cart.toDate+"','"+cart.maxQuantity+"')";
            sql.query(quer, function(error, response){
                if(error){
                    console.log("error :",error);
                    result(error,null);
                }else{
                    result(null,response);                   
                }
            });

        }
    });
}
Cart.addToCartForUser = function (cart,result){
    var i;
      let arraySoftwares = []
      let resfinal ;
      que = "select softwareid from software where softwaredesc in ('"
      for (var i = 0; i< cart.softwares.length; i++){
          if(i>0){
              que += ",'"
          }
          que += cart.softwares[i]+"'"
      }
      que += ")"

        sql.query(que, function(error, response){
        if(error){
            console.log("error :",error);
            result(error,null);
        }else{
            //result(null,response);
            swId = response[0].softwareid;
            arraySoftwares = response;
           

            
            let q =""
            for(var j=0;j<arraySoftwares.length;j++){
                q += "insert into cartdetails(cartid,softwareid) values('"+ cart.cartId+"','"+arraySoftwares[j].softwareid+"'); "; 
            }
            sql.query(q, function(err, res){
            if(err){
                console.log("error :",err);
                result(err,null);
            }else{
                result(null,res);
            }
            });
        
        }
    });
    

   
}

    
    
   


Cart.getCartDetailsForUser = function (cart,result){
    sql.query("select cart.quantity,cart.maxQuantity,cart.productID,cart.fromDate,cart.toDate, "
    +"cart.cartid,product.Company,product.Product,product.TypeName,product.Inches, "
    +"product.perDay,product.productImg,GROUP_CONCAT(softwaredesc) as softwares from cart "
    +"inner join product on cart.productID = product.productID "
    +"inner join cartdetails on cart.cartid = cartdetails.cartid "
    +"inner join software on cartdetails.softwareid = software.softwareid where uid= ? "
    +"group by cart.productID,cart.fromDate,cart.toDate, "
    +"cart.cartid,product.Company,product.Product,product.TypeName,product.Inches, "
    +"product.perDay,product.productImg,cart.quantity,cart.maxQuantity ",cart.uid, function(error, response){

        if(error){
            console.log("error :",error);
            result(error,null);
        }else{
            result(null,response)
        }
    })
}


Cart.removeCartItem = function (cart, result){
   
    sql.query("delete from cart where cartid="+cart.cartId+" and uid="+cart.uid, function(error, response){
        if(error){
            console.log("error :",error);
            result(error,null);
        }else{
            //result(null,response)
            sql.query("delete from cartdetails where cartid= ?",cart.cartId, function(err,res){
                if(err){
                    result(err,null);
                }else{
                    result(null,res)
                }
            })
        }
    })
}

Cart.updateCartItem = function (cart, result){
    qu = "update cart set "
   

    if(cart.quantity!='' && cart.quantity!=undefined){
        qu += "quantity="+cart.quantity+" "
    }
    if(cart.fromDate!='' && cart.fromDate!=undefined){
        if(cart.quantity!='' && cart.quantity!=undefined){
            qu += ","
        }
        qu += " fromDate = '"+cart.fromDate+"' "
    }
    if(cart.toDate!='' && cart.toDate != undefined){
        if((cart.quantity!='' && cart.quantity!=undefined) || (cart.fromDate!='' && cart.fromDate!=undefined) ){
            qu += ","
        }
        qu += " toDate = '"+cart.toDate+"' "
    }
    qu+=" where cartid="+cart.cartId
   
    sql.query(qu, function(error, response){
        if(error){
            console.log("error :",error);
            result(error,null);
        }else{
            result(null,response)
        }
    })

}

Cart.updateSoftware = function (cart,result){
    var i;
    for (i = 0; i < cart.softwares.length; i++) {
      sql.query("select softwareid from software where softwaredesc = ?", cart.softwares[i], function(error, response){
        if(error){
            console.log("error :",error);
            result(error,null);
        }else{
            swId = response[0].softwareid;
            var q = "insert into cartdetails(cartid,softwareid) values('"+ cart.cartId+"','"+swId+"')";
            sql.query(q, function(e,r){
                if(e){
                    console.log("error :",e);
                    result(e,null);
                }else{       
                }
            })
        }
      })
    }
    result(null,result);
}


Cart.getCartCount =function(cart,result){
    sql.query("select count(*) as c from cart where uid=?",cart.uid, function(error, response){
        if(error){
            console.log("error :",error);
            result(error,null);
        }else{       
            result(null,response);
        }
    })
}

Cart.getRandomPromotions = function(result){
    sql.query("select product.*,inventory.availabilty from product, "
    +"inventory  where ( (TypeName = 'Notebook') "
    +"  or (TypeName = 'Gaming') or (TypeName = 'Workstation') ) and " 
    +"product.productID = inventory.productID "
    +"ORDER BY RAND() LIMIT 3;", function(error,response){
        if(error){
            console.log("error :",error);
            result(error,null);
        }else{    
               
            result(null,response);
        }
    })
}


module.exports= Cart;