/*
    Author: Thanigaiselvan senthil shanmugam - Product Page
    Author: Hari Arunachalam - Search functionality
*/
var sql = require('../../db.js');


var ProductD = function (product) {
    this.productID = product.productID;
    this.Company = product.Company;
    this.Product = product.Product;
    this.TypeName = product.TypeName;
    this.Inches = product.Inches;
    this.ScreenResolution = product.ScreenResolution;
    this.Cpu = product.Cpu;
    this.Ram = product.Ram;
    this.Memory = product.Memory;
    this.Gpu = product.Gpu;
    this.OpSys = product.OpSys;
    this.Weight = product.Weight;
    this.Price = product.Price;
    this.isDeleted = product.isDeleted;
    this.isFeatured = product.isFeatured;
    this.perDay = product.perDay;
    this.fromDate = product.fromDate;
    this.quantity = product.quantity;
}

//Retrieve product details to display in the  Product page
ProductD.getProductDetails = function (productId, result) {
    sql.query("Select * from product where productID = ? ;", productId, function (err, res) {
        if (err) {
            result(err, null);
        }
        else {
            result(null, res);

        }

    });
}
//Retrieve product id
ProductD.getProductID = function (product, result) {
    sql.query("select productID from product where Product = ? limit 1;", product, function (err, res) {
        if (err) {
            result(err, null);
        }
        else {
            result(null, res);

        }

    });
}

//Retrieve from date
ProductD.getFromDate = function (productId, result) {
    sql.query("select min(fromDate) as fromDate from inventory where productId= ? and  (toDate is null or toDate='') and availabilty=1;", productId, function (err, resp) {
        if (err) {
            result(err, null);
        }
        else {
            result(null, resp);

        }
    });
}

ProductD.search = function (text, start, end, result) {

    let q = "select distinct(Product),Company from product inner join inventory on product.productID = inventory.productID" +
        " where (Product like \"%" + text + "%\" or Company like \"%" + text + "%\") and inventory.availabilty=1 and product.isDeleted=0 order by Product asc limit " + start + "," + end + ";"
    sql.query(q, function (err, resp) {
        if (err) {
            result(err, null);
        }
        else {
            result(null, resp);

        }
    });
}


//Retrieve quantity
ProductD.getQuantity = function (productId, result) {
    sql.query("select  count(inventory.productID) as quantity from product inner join inventory on product.productID = inventory.productID where product.productID= ? and product.isDeleted=0 and inventory.availabilty =1 group by inventory.productID;", productId, function (err, resp) {
        if (err) {
            result(err, null);
        }
        else {
            result(null, resp);

        }
    });
}

ProductD.filter = function (text, processors, ram, memory, resolution, Company, start, end, result) {
    firstFilter = false;

    used = false
    qBeg = "select * from product where product.productID in " +
        " (select product.productID from product  inner join inventory on product.productID=inventory.productID " +
        " where (Product like \"%" + text + "%\" or Company like \"%" + text + "%\") "
    for (i = 0; i < processors.length; i++) {
        if (firstFilter) {
            firstFilter = false
            qBeg = qBeg + "and ( Cpu like \"%" + processors[i] + "%\""
        }
        else if (i == 0) {
            qBeg = qBeg + "and ( Cpu like \"%" + processors[i] + "%\""
        } else {
            qBeg = qBeg + " or Cpu like \"%" + processors[i] + "%\""
        }
        if (i == processors.length - 1) {
            qBeg = qBeg + ")"
        }
    }


    for (i = 0; i < ram.length; i++) {
        if (firstFilter) {
            firstFilter = false
            qBeg = qBeg + "and ( Ram like \"%" + ram[i] + "%\""
        }
        if (i == 0) {
            qBeg = qBeg + "and ( Ram like \"%" + ram[i] + "%\""
        } else {
            qBeg = qBeg + " or Ram like \"%" + ram[i] + "%\""
        }

        if (i == ram.length - 1) {
            qBeg = qBeg + ")"
        }
    }

    for (i = 0; i < memory.length; i++) {
        if (firstFilter) {
            firstFilter = false
            qBeg = qBeg + "and ( Memory like \"%" + memory[i] + "%\""
        }
        if (i == 0) {
            first = false;
            qBeg = qBeg + " and ( Memory like \"%" + memory[i] + "%\""
        } else {
            qBeg = qBeg + " or Memory like \"%" + memory[i] + "%\""
        }

        if (i == memory.length - 1) {
            qBeg = qBeg + ")"
        }
    }

    for (i = 0; i < resolution.length; i++) {
        if (firstFilter) {
            firstFilter = false
            qBeg = qBeg + "and ( ScreenResolution like \"%" + resolution[i] + "%\""
        }
        if (i == 0) {
            first = false;
            qBeg = qBeg + " and ( ScreenResolution like \"%" + resolution[i] + "%\""
        } else {
            qBeg = qBeg + " or ScreenResolution like \"%" + resolution[i] + "%\""
        }
        if (i == resolution.length - 1) {
            qBeg = qBeg + ")"
        }
    }

    for (i = 0; i < Company.length; i++) {
        if (firstFilter) {
            firstFilter = false
            qBeg = qBeg + "and ( Company like \"%" + Company[i] + "%\""
        }
        if (i == 0) {
            first = false;
            qBeg = qBeg + " and ( Company like \"%" + Company[i] + "%\""
        } else {
            qBeg = qBeg + " or Company like \"%" + Company[i] + "%\""
        }
        if (i == Company.length - 1) {
            qBeg = qBeg + ")"
        }
    }
    qCount = qBeg;

    qCount = qCount.replace("*", "count(*) as c")

    qEnd = "and inventory.availabilty=1 and product.isDeleted = 0) order by Product asc limit " + start + " offset " + end + ";"
    qBeg = qBeg + qEnd;
    qCount = qCount + "and inventory.availabilty=1 and product.isDeleted = 0) order by Product;"
    qCount = qCount + qBeg;
    sql.query(qCount, function (err, resp) {
        if (err) {
            result(err, null);
        }
        else {
            result(null, resp);

        }

    });
}

ProductD.addProduct = function (req, result) {
    sql.query("select  max(productID) as c from product", function (err, resp) {
        if (err) {
            result(err, null);
        }
        else {
            let productID = resp[0].c + 1;
            let q = "insert into product values(" +
                productID + ",'" +
                req.body.laptopForm.Company + "\',\'" +
                req.body.laptopForm.Product + "\',\'" +
                req.body.laptopForm.TypeName + "\'," +
                req.body.techForm.Inches + ",'" +
                req.body.techForm.ScreenResolution + "\',\'" +
                req.body.techForm.Cpu + "\',\'" +
                req.body.techForm.Ram + "\',\'" +
                req.body.techForm.Memory + "\',\'" +
                req.body.techForm.Gpu + "\',\'" +
                req.body.techForm.OpSys + "\'," +
                req.body.laptopForm.Weight + "," +
                req.body.laptopForm.Price + ",0,0,'" + req.body.laptopForm.Price + "\',\'" +
                req.body.laptopForm.productImg + "');"



            sql.query(q, function (err, resp) {
                if (err) {
                    result(err, null);
                }
                else {
                    
                    sql.query("select max(inventoryID) as c from inventory", function (err, resp) {
                        if (err) {
                            result(err, null);
                        }
                        else {
                            let inventoryID = resp[0].c + 1;
                            let q = "insert into inventory values(" +
                                inventoryID + "," +
                                productID + ",1,'2020-03-31',null);"
                            sql.query(q, function (err, resp) {
                                if (err) {
                                    result(err, null);
                                }
                                else {
                                    result(null, resp);
                                }
                            });


                        }
                    });



                }
            });


        }
    });



}

module.exports = ProductD;