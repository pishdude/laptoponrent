/*
    Author: Srikrishna Sasidharan
*/

let jwt = require('jsonwebtoken');
let config = require('../../config/database.config.js');


var AddressModel = require('../models/address.model')

//add address for the user uid.
exports.addAddress = (req,res)=>{
    class Address{
        constructor(){
            this.firstname = req.body.firstname;
            this.lastname = req.body.lastname;
            this.phone = req.body.phone;
            this.uid = req.body.uid;
            this.addressId = req.body.addressId;
            this.addressline1 = req.body.addressline1;
            this.addressline2 = req.body.addressline2;
            this.city = req.body.city;
            this.state = req.body.state;
            this.postcode = req.body.postcode;
        }
    }
    var address = new Address();
    AddressModel.addNewAddress(address, function(err, task){
        if(task){
            res.json({
                success: true,
                message: 'Address inserted.'
              });
        }else{
            res.json({
                success: false,
                message: 'Error while inserting address.'
              });
        }
    })
}

//get all user addresses
exports.getAddressForUser = (req, res) => {
    class Address{
        constructor(){
            this.firstname = req.body.firstname;
            this.lastname = req.body.lastname;
            this.phone = req.body.phone;
            this.uid = req.body.uid;
            this.addressId = req.body.addressId;
            this.addressline1 = req.body.addressline1;
            this.addressline2 = req.body.addressline2;
            this.city = req.body.city;
            this.state = req.body.state;
            this.postcode = req.body.postcode;
        }
    }
    var address = new Address();
    AddressModel.getAddressForUser(address, function(err, task){
        var addressItems=[]
        for (var i = 0; i < task.length; i++){
            addressItems[i] = task[i]
        }
        if(task){
            res.json({
                success: true,
                address: addressItems
              });
        }else{
            res.json({
                success: false,
                message: 'Error while retrieving address details.'
              });
        }
    })
}

// remove user address
exports.removeAddress=(req,res)=>{
    class Address{
        constructor(){
            this.firstname = req.body.firstname;
            this.lastname = req.body.lastname;
            this.phone = req.body.phone;
            this.uid = req.body.uid;
            this.addressId = req.body.addressId;
            this.addressline1 = req.body.addressline1;
            this.addressline2 = req.body.addressline2;
            this.city = req.body.city;
            this.state = req.body.state;
            this.postcode = req.body.postcode;
        }
    }
    var address = new Address();
    AddressModel.removeAddress(address, function(error,task){
        if(task){
            res.json({
                success: true,
                message: 'Address removed.'
              });
        }else{
            res.json({
                success: false,
                message: 'Error while removing address.'
              });
        }
    })
}

//update user address
exports.editAddress =(req, res)=>{
    class Address{
        constructor(){
            this.firstname = req.body.firstname;
            this.lastname = req.body.lastname;
            this.phone = req.body.phone;
            this.uid = req.body.uid;
            this.addressId = req.body.addressId;
            this.addressline1 = req.body.addressline1;
            this.addressline2 = req.body.addressline2;
            this.city = req.body.city;
            this.state = req.body.state;
            this.postcode = req.body.postcode;
        }
    }
    var address = new Address();
    AddressModel.editAddress(address,function(error,task){
        if(task){
            res.json({
                success: true,
                message: 'Address updated.'
              });
        }else{
            res.json({
                success: false,
                message: 'Error while updating address.'
              });
        }
    })
}