/*
    Author: Sreyas Naaraayanan Ramanathan
*/

//let jwt = require('jsonwebtoken');
let config = require('../../config/database.config.js');

var UserModel = require('../models/registerUser.model');

exports.registerUser = (req, res) => {
    class User {
        constructor(){

            this.uId = req.body.uId;
            this.firstName = req.body.firstName;
            this.lastName = req.body.lastName;
            this.email = req.body.email;
            this.password = req.body.password;
            this.addressLine1 = req.body.addressLine1;
            this.addressLine2 = req.body.addressLine2;
            this.city = req.body.city;
            this.state = req.body.state;
            this.postcode = req.body.postcode;
            this.phoneNumber = req.body.phoneNumber;
            this.isAdmin =req.body.isAdmin;
            this.isLocked =req.body.isLocked;
        }


    }

    user = new User()
    // Register User to application
    UserModel.addUser(user,function(err,regUsers){


        if(regUsers){
            res.json({
                success: true,
                message: 'Users Created'
              });
        }
        else if(regUsers == null){
            res.json({
                success: 'alreadyRegistered',
                message: 'Users already exist .Please Login'
              });

        }       
        
        else {
            res.json({
              success: false,
              message: 'Error occured while registration'
            });
          }


    });

}