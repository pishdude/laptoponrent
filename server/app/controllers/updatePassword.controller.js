/*
    Author: Sreyas Naaraayanan Ramanathan
*/

var sql = require('../../db.js');
var CryptoJS = require("crypto-js");
let config = require('../../config/database.config.js');

var UserDetails = require('../models/updatePassword.model');

exports.updatePassword = (req, res) => {

    class UserDetail {
        constructor() {
            this.username = req.body.username;
            this.otp = req.body.otp;
            this.password = req.body.password;
            this.confirmPassword = req.body.confirmPassword;
        }

    }
    user = new UserDetail()
    // Reset/Update password 
    UserDetails.updatePassword(user, function (err, task) {


        if (err) {

            res.send(err);
        }

        if (task) {

            res.json({
                success: true,
                message: 'New Password Updated in table'
            });
        }
        else if (task == null) {
            res.json({
                success: 'wrongOTP',
                message: 'Incorrect OTP provided try again'
            });

        }
        else {
            res.json({
                success: false,
                message: 'Error occurred in reseting Password.Try Again'
            });
        }
    });
}