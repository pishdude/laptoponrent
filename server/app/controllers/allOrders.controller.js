/*
    Author: Sreyas Naaraayanan Ramanathan
*/

let jwt = require('jsonwebtoken');
let config = require('../../config/database.config.js');

var PastOrders = require('../models/allOrders.model');

exports.allOrders= (req,res) =>{

    class PastOrderDetails {
        constructor() {
            this.uid = req.body.uid;
            
        }

    }
    fetchAllOrders = new PastOrderDetails()
    //Fetch All orders from Db
    PastOrders.allOrders(fetchAllOrders,function (err,task){   


        if (err) {
            res.send(err);
        }
        if (task) {
            res.json({
                success: true,
                message: 'Order details fetched successfully',
                data: task

            });
        }
        else {
            res.json({
                success: false,
                message: 'Error occured while fectching details'
            });
        }



    });


}