/*
    Author: Sreyas Naaraayanan Ramanathan,Hari Arunachalam
*/

let config = require('../../config/database.config.js');

var UpdateUserDetails = require('../models/editProfileDb.model');

exports.editProfileDb = (req, res) => {

    class UserDetail {
        constructor() {
            this.uId = req.body.uId;
            this.firstName = req.body.firstName;
            this.lastName = req.body.lastName;
            this.addressLine1 = req.body.addressLine1;
            this.addressLine2 = req.body.addressLine2;
            this.phoneNumber = req.body.phoneNumber;
            this.city = req.body.city;
            this.state = req.body.state;
            this.postcode = req.body.postcode;
        }

    }
    user = new UserDetail()

    UpdateUserDetails.editProfileDb(user, function (err, task) {

        
        if (err) {
            res.send(err);
        }
        if (task) {
            res.json({
                success: true,
                message: 'Details fetched successfully',
                data: task

            });
        }
        else {
            res.json({
                success: false,
                message: 'Error occured while updating details'
            });
        }
    });
}
//Lock/unlock users
exports.lockUser=(req,res)=>{
     var uId = req.body.uId;
     var isLocked = req.body.isLocked;

     UpdateUserDetails.lockUser(uId,isLocked,function(err,task){
        if (err) {
            res.send(err);
        }
        if (task) {
            res.json({
                success: true,
                // message: 'Details fetched successfully',
                data: task

            });
        }
     })
}